package main

import "gitlab.com:kirodrow2/goLanding/internal/app"
const configPath = "configs/main"

func main() {
	app.Run(configPath)
}
